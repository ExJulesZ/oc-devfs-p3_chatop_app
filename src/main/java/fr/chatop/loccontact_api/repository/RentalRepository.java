package fr.chatop.loccontact_api.repository;

import fr.chatop.loccontact_api.model.Rental;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface RentalRepository extends CrudRepository<Rental, Long> { }
