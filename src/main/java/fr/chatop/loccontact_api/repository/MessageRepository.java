package fr.chatop.loccontact_api.repository;

import fr.chatop.loccontact_api.model.Message;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface MessageRepository extends CrudRepository<Message, Long> { }
