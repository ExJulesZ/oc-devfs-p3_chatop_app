package fr.chatop.loccontact_api.controller;

import fr.chatop.loccontact_api.service.MessageService;
import fr.chatop.loccontact_api.model.response.MessageResponse;
import fr.chatop.loccontact_api.model.request.MessageRequest;
import fr.chatop.loccontact_api.model.Message;

import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.v3.oas.annotations.Operation;
import javax.validation.Valid;

import java.sql.Timestamp;

@RestController
@RequestMapping("api/messages")
@Tag(name = "Message Controller", description = "Endpoints about users messages for a rental.")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @PostMapping(path="")
    @Operation(summary = "Create a new message", description = "With a message text (string), the id of sender user (int) and the id of selected rental (int) in JSON, create a new Message in database and return the result (\"Saved.\" / \"Unsaved.\") at \"message\" key.")
    public MessageResponse messages(@RequestBody @Valid MessageRequest messageRequest) {

        Timestamp now = new Timestamp(System.currentTimeMillis());

        Message theMessage = new Message();
        theMessage.setRental_id(messageRequest.getRentalId());
        theMessage.setUser_id(messageRequest.getUserId());
        theMessage.setMessage(messageRequest.getMessage());
        theMessage.setCreated_at(now);
        theMessage.setUpdated_at(now);

        Message newMessage = messageService.saveMessage(theMessage);

        if(newMessage!=null && newMessage.getId()>0) {
            return new MessageResponse("Saved.");
        } else {
            return new MessageResponse("Unsaved.");
        }
    }
}
