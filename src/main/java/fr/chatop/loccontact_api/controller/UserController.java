package fr.chatop.loccontact_api.controller;

import fr.chatop.loccontact_api.configuration.EncoderConfig;
import fr.chatop.loccontact_api.service.UserService;
import fr.chatop.loccontact_api.configuration.UserDetailsServiceImpl;
import fr.chatop.loccontact_api.configuration.JwtTokenUtil;
import fr.chatop.loccontact_api.model.request.JwtRequest;
import fr.chatop.loccontact_api.model.request.UserRequest;
import fr.chatop.loccontact_api.model.User;
import fr.chatop.loccontact_api.model.response.JwtResponse;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.v3.oas.annotations.Operation;
import javax.validation.Valid;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.BadCredentialsException;

import java.sql.Timestamp;
import java.util.Optional;

@RestController
@RequestMapping("api")
@Tag(name = "User Controller", description = "Endpoints about users and currently logged in user using JWT.")
public class UserController {

    @Autowired
    private EncoderConfig encoderConfig;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @PostMapping(path="/auth/login")
    @Operation(summary = "Log in user and return JWT", description = "With an email (string) and a password (string) in JSON, if user exists and credentials corrects, log in this user and return JWT code (string) at \"token\" key.")
    public ResponseEntity<?> login(@RequestBody JwtRequest authenticationRequest) throws Exception {

        return authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());

    }

    @PostMapping(path="/auth/register")
    @Operation(summary = "Create a new user, log in it and return JWT", description = "With an email (string), a name (string), and a password (string) in JSON, create a new User (user role) in database, log in this new user and return JWT code (string) at \"token\" key.")
    public @ResponseBody ResponseEntity<?> register(@RequestBody @Valid UserRequest userRequest) throws Exception {

        String email = userRequest.getEmail();
        String name = userRequest.getName();
        String password = userRequest.getPassword();

        PasswordEncoder encoder = encoderConfig.passwordEncoder();
        String encodedPassword = encoder.encode(password);

        Timestamp now = new Timestamp(System.currentTimeMillis());

        User newUser = new User();
        newUser.setName(name);
        newUser.setEmail(email);
        newUser.setPassword(encodedPassword);
        newUser.setCreated_at(now);
        newUser.setUpdated_at(now);

        userService.saveUser(newUser);

        return authenticate(email, password);
    }

    @GetMapping(path="/auth/me")
    @Operation(summary = "Get logged in user", description = "Return the currently logged in user (User object).")
    public UserDetails me(Authentication authentication) {
        return userService.loadUserByUsername(authentication.getName());
    }

    @GetMapping(path="/user/{id}")
    @Operation(summary = "Get user by id", description = "With an id (int) in the URL, if user exists, return this user (User object).")
    public Optional<User> userGet(@PathVariable Long id) {
        return userService.getUser(id);
    }

    private ResponseEntity<?> authenticate(String login, String password) throws Exception {

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(login);
        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(token));
    }
}
