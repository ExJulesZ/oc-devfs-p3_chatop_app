package fr.chatop.loccontact_api.controller;

import fr.chatop.loccontact_api.service.RentalService;
import fr.chatop.loccontact_api.service.UserService;
import fr.chatop.loccontact_api.service.FileUploadService;
import fr.chatop.loccontact_api.model.response.RentalsResponse;
import fr.chatop.loccontact_api.model.Rental;
import fr.chatop.loccontact_api.model.response.RentalResponse;
import fr.chatop.loccontact_api.model.request.RentalCreateRequest;
import fr.chatop.loccontact_api.model.request.RentalRequest;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.v3.oas.annotations.Operation;
import javax.validation.Valid;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.security.core.Authentication;

@RestController
@RequestMapping("api/rentals")
@Tag(name = "Rental Controller", description = "Endpoints about the publication of rentals.")
public class RentalController {

    @Autowired
    private RentalService rentalService;

    @Autowired
    private UserService userService;

    @Autowired
    private FileUploadService fileUploadService;

    @GetMapping(path="")
    @Operation(summary = "Get all rentals", description = "Return all rentals (list of Rental objects) at \"rentals\" key.")
    public RentalsResponse rentalsGetAll() {
        return rentalService.getRentals();
    }

    @GetMapping(path="/{id}")
    @Operation(summary = "Get rental by id", description = "With an id (int) in the URL, if rental exists, return this rental (Rental object).")
    public Optional<Rental> rentalsGet(@PathVariable Long id) {
        return rentalService.getRental(id);
    }

    @PostMapping(path="")
    @Operation(summary = "Create a new rental", description = "With a name (string), a surface (float to string), a price (float to string), a picture (file) and a description (string) in form-data body, create a new Rental in database and return the result (\"Saved.\" / \"Unsaved.\") at \"message\" key.")
    public RentalResponse rentalsCreate(@ModelAttribute @Valid RentalCreateRequest rentalRequest, Authentication authentication, HttpServletRequest request) throws IOException {

        BigDecimal theSurface = BigDecimal.valueOf(Double.parseDouble(rentalRequest.getSurface()));
        BigDecimal thePrice = BigDecimal.valueOf(Double.parseDouble(rentalRequest.getPrice()));
        int ownerId = userService.myId(authentication).intValue();

        String baseUrl = ServletUriComponentsBuilder
                .fromRequestUri(request)
                .replacePath(null)
                .build()
                .toUriString();

        String pictureFileName = fileUploadService.saveFile(rentalRequest.getPicture());
        String pictureUrl = baseUrl + "/" + pictureFileName;

        Timestamp now = new Timestamp(System.currentTimeMillis());

        Rental rental = new Rental();
        rental.setName(rentalRequest.getName());
        rental.setSurface(theSurface);
        rental.setPrice(thePrice);
        rental.setPicture(pictureUrl);
        rental.setDescription(rentalRequest.getDescription());
        rental.setOwner_id(ownerId);
        rental.setCreated_at(now);
        rental.setUpdated_at(now);

        Rental newRental = rentalService.saveRental(rental);

        if(newRental!=null && newRental.getId()>0) {
            return new RentalResponse("Saved.");
        } else {
            return new RentalResponse("Unsaved.");
        }
    }

    @PutMapping(path="/{id}")
    @Operation(summary = "Update rental by id", description = "With an id (int) in the URL, with a name (string), a surface (float to string), a price (float to string) and a description (string) in form-data body, if rental exists, update this Rental in database and return result (\"Updated.\") at \"message\" key, else if rental does not exist, return result (\"Unupdated...\") at \"message\" key.")
    public RentalResponse rentalsUpdate(@PathVariable Long id, @ModelAttribute @Valid RentalRequest rentalRequest, Authentication authentication) {

        BigDecimal theSurface = BigDecimal.valueOf(Double.parseDouble(rentalRequest.getSurface()));
        BigDecimal thePrice = BigDecimal.valueOf(Double.parseDouble(rentalRequest.getPrice()));
        int ownerId = userService.myId(authentication).intValue();

        Optional<Rental> currRental = rentalService.getRental(id);
        if(currRental.isPresent()) {

            String thePicture = currRental.get().getPicture();

            Timestamp createdAt = currRental.get().getCreated_at();

            Rental rental = new Rental();
            rental.setId(id);
            rental.setName(rentalRequest.getName());
            rental.setSurface(theSurface);
            rental.setPrice(thePrice);
            rental.setPicture(thePicture);
            rental.setPicture(currRental.get().getPicture());
            rental.setDescription(rentalRequest.getDescription());
            rental.setOwner_id(ownerId);
            rental.setCreated_at(createdAt);
            rental.setUpdated_at(new Timestamp(System.currentTimeMillis()));

            rentalService.updateRental(rental);

            return new RentalResponse("Updated.");
        } else {
            return new RentalResponse("Unupdated : no rental with id "+id.toString()+".");
        }
    }
}
