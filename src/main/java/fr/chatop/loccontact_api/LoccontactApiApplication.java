package fr.chatop.loccontact_api;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;

@SpringBootApplication
public class LoccontactApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoccontactApiApplication.class, args);
	}

}
