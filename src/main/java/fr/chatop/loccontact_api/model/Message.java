package fr.chatop.loccontact_api.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "messages")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int rental_id;

    private int user_id;

    private String message;

    private Timestamp created_at;

    private Timestamp updated_at;

}
