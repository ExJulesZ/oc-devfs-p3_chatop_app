package fr.chatop.loccontact_api.model.response;

import fr.chatop.loccontact_api.model.Rental;

import java.io.Serial;
import java.io.Serializable;

public class RentalsResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = -2956300147985632100L;

    private final Iterable<Rental> rentals;

    public RentalsResponse(Iterable<Rental> rentalsArr) {
        this.rentals = rentalsArr;
    }

    public Iterable<Rental> getRentals() {
        return this.rentals;
    }
}

