package fr.chatop.loccontact_api.model.response;

import java.io.Serial;
import java.io.Serializable;

public class MessageResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = -8763104578424005001L;

    private final String message;

    public MessageResponse(String aMessage) {
        this.message = aMessage;
    }

    public String getMessage() {
        return this.message;
    }
}
