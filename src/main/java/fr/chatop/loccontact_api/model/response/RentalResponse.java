package fr.chatop.loccontact_api.model.response;

import java.io.Serial;
import java.io.Serializable;

public class RentalResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = -1367852145921555488L;

    private final String message;

    public RentalResponse(String aMessage) {
        this.message = aMessage;
    }

    public String getMessage() {
        return this.message;
    }
}
