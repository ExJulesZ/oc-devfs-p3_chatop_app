package fr.chatop.loccontact_api.model.request;

import java.io.Serial;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import java.io.Serializable;

public class MessageRequest implements Serializable {

    @Serial
    private static final long serialVersionUID = 1125478930025486214L;

    @NotNull
    @Min(1)
    private int rental_id;

    @NotNull
    @Min(1)
    private int user_id;

    @NotEmpty
    private String message;


    public MessageRequest() { }

    public MessageRequest(int rental_id, int user_id, String message) {
        this.setRentalId(rental_id);
        this.setUserId(user_id);
        this.setMessage(message);
    }


    public int getRentalId() {
        return this.rental_id;
    }

    public void setUserId(int user_id) {
        this.user_id = user_id;
    }

    public int getUserId() {
        return this.user_id;
    }

    public void setRentalId(int rental_id) {
        this.rental_id = rental_id;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
