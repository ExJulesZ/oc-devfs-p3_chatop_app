package fr.chatop.loccontact_api.model.request;

import java.io.Serial;
import javax.validation.constraints.NotEmpty;

import java.io.Serializable;

public class RentalRequest implements Serializable {

    @Serial
    private static final long serialVersionUID = 4251548969963254447L;

    @NotEmpty
    private String name;

    @NotEmpty
    //String because it's a request part
    private String surface;

    @NotEmpty
    //String because it's a request part
    private String price;

    @NotEmpty
    private String description;


    public RentalRequest() { }

    public RentalRequest(String name, String surface, String price, String description) {
        this.setName(name);
        this.setSurface(surface);
        this.setPrice(price);
        this.setDescription(description);
    }


    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurface() {
        return this.surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
