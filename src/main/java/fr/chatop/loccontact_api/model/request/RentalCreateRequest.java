package fr.chatop.loccontact_api.model.request;

import javax.validation.constraints.NotNull;
import org.springframework.web.multipart.MultipartFile;

public class RentalCreateRequest extends RentalRequest {

    @NotNull
    private MultipartFile picture;


    public RentalCreateRequest() { }

    public RentalCreateRequest(MultipartFile picture) {
        super();
        this.setPicture(picture);
    }


    public MultipartFile getPicture() {
        return this.picture;
    }

    public void setPicture(MultipartFile picture) {
        this.picture = picture;
    }
}
