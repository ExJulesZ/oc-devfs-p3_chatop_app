package fr.chatop.loccontact_api.model.request;

import java.io.Serial;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Email;

import java.io.Serializable;

public class UserRequest implements Serializable {

    @Serial
    private static final long serialVersionUID = 1112489687969787702L;

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    private String name;

    @NotEmpty
    private String password;


    public UserRequest() { }

    public UserRequest(String email, String name, String password) {
        this.setEmail(email);
        this.setName(name);
        this.setPassword(password);
    }


    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
