package fr.chatop.loccontact_api.service;

import fr.chatop.loccontact_api.repository.UserRepository;
import fr.chatop.loccontact_api.configuration.UserDetailsServiceImpl;
import fr.chatop.loccontact_api.model.User;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.Authentication;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    public void saveUser(User user) {
        userRepository.save(user);
    }

    public @ResponseBody Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    public Optional<User> getUser(final Long id) {
        return userRepository.findById(id);
    }

    public Optional<User> getUser(final String email) {
        return userRepository.findByEmail(email);
    }

    public void updateUser(User user) {
        this.saveUser(user);
    }

    public void deleteUser(final Long id) {
        userRepository.deleteById(id);
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userDetailsService.loadUserByUsername(username);
    }

    public Long myId(Authentication authentication) {
        return getUser(authentication.getName()).get().getId();
    }

}
