package fr.chatop.loccontact_api.service;

import fr.chatop.loccontact_api.repository.MessageRepository;
import fr.chatop.loccontact_api.model.Message;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;

    public Message saveMessage(Message message) {
        return messageRepository.save(message);
    }

    public @ResponseBody Iterable<Message> getMessages() {
        return messageRepository.findAll();
    }

    public Optional<Message> getMessage(final Long id) {
        return messageRepository.findById(id);
    }

    public void updateMessage(Message message) {
        this.saveMessage(message);
    }

    public void deleteMessage(final Long id) {
        messageRepository.deleteById(id);
    }

}
