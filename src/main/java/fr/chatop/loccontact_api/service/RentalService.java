package fr.chatop.loccontact_api.service;

import fr.chatop.loccontact_api.repository.RentalRepository;
import fr.chatop.loccontact_api.model.Rental;
import fr.chatop.loccontact_api.model.response.RentalsResponse;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Service
public class RentalService {

    @Autowired
    private RentalRepository rentalRepository;

    public Rental saveRental(Rental rental) {
        return rentalRepository.save(rental);
    }

    public RentalsResponse getRentals() {
        return new RentalsResponse(rentalRepository.findAll());
    }

    public Optional<Rental> getRental(final Long id) {
        return rentalRepository.findById(id);
    }

    public void updateRental(Rental rental) {
        this.saveRental(rental);
    }

    public void deleteRental(final Long id) {
        rentalRepository.deleteById(id);
    }

}
