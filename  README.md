# P3 : LocContact


## Installer l'API

1. Récupérer l'API sur Gitlab. Vérifier d'être bien sur la branche main. Cliquer sur "clone" et copier l'adresse HTTPS.

2. A l'emplacement de son choix ouvrir un cmd. Cloner le projet avec 'git clone url_copié'.

3. Ouvrir le projet 'oc-devfs-p3_loccontact_api' avec IntelliJ (par exemple).


## Installer l'application

1. A l'emplacement de son choix ouvrir un cmd. Cloner le projet avec 'git clone https://gitlab.com/ExJulesZ/oc-devfs-p3_loccontact_app'.


## Créer la base de données

1. Lancer son serveur MySQL avec par exemple Wamp (ou équivalent).

2. Si besoin accéder à l'interface phpmyadmin (ou équivalent).

3. Se rendre à oc-devfs-p3_loccontact_app\ressources\sql et exécuter le script sql.


## Vérifier la configuration de l'API

Se rendre à oc-devfs-p3_chatop_app\src\main\resources, ouvrir application.properties et vérifier la configuration, notamment :
   - comme vu à la ligne 2 l'API utilise le port 3001, s'assurer de ne pas l'utiliser ailleurs ;
   - ligne 6, vérifier le port utilisé par MySQL et modifier cette ligne ci besoin, vérifier également le nom de la base de données en cas de problèmes futurs ;
   - lignes 7 et 8, en cas de problèmes futurs vérifier le nom d'utilsateur et le mot de passe (utilisateur de base de données).


## Démarrer le projet

1. Démarrer l'API : Si utilisation d'IntelliJ (ou équivalent), clic droit sur 'LoccontactApiApplication' et cliquer sur 'Run ...'.

2. Démarrer l'application : Accéder à l'application avec 'cd oc-devfs-p3_loccontact_app' dans un cmd.

3. Installer les dépendances avec 'npm i'. Et vérifier ne pas avoir de problèmes de version.

4. Enfin lancer l'application avec 'npm run start'. Attendre la génération puis dans le navigateur accéder à localhost:4200.


## Consulter la documentation

1. Dans le navigateur accéder à http://localhost:3001.

2. Se connecter ou s'enregistrer.

3. Accéder aux docs aux adresses http://localhost:3001/v3/api-docs et http://localhost:3001/swagger-ui/index.html.
